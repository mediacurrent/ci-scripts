#!/bin/bash

# Check if the 'acli' command is available
if ! command -v acli &> /dev/null; then
  apt-get update
  apt-get install jq -y
  curl -OL https://github.com/acquia/cli/releases/latest/download/acli.phar
  chmod +x acli.phar
  mv acli.phar /usr/local/bin/acli
  if ! command -v acli &> /dev/null; then
    echo "Error setting up ACLI."
    exit 1;
  fi
fi

acli -n auth:login -n --key="${ACQUIA_API_KEY}" --secret="${ACQUIA_API_SECRET}"

# Run acli command to clone the instance
acli app:task-wait "$(acli api:environments:database-copy $ENV_TO $DB_NAME $ENV_FROM -n)"

# Check the exit status of the previous command
if [ $? -eq 0 ]; then
    echo "Instance cloned successfully."
else
    echo "Error: Instance cloning failed."
    exit 1;
fi

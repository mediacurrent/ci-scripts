#!/bin/bash

# Check if the 'acli' command is available
if ! command -v acli &> /dev/null; then
  apt-get update
  apt-get install jq -y
  curl -OL https://github.com/acquia/cli/releases/latest/download/acli.phar
  chmod +x acli.phar
  mv acli.phar /usr/local/bin/acli
  if ! command -v acli &> /dev/null; then
    echo "Error setting up ACLI."
    exit 1;
  fi
fi

acli auth:login --key="${ACQUIA_API_KEY}" --secret="${ACQUIA_API_SECRET}" -n

echo "Switching codebase on $ENV_TO."

ACLI=$(acli api:environments:code-switch $ENV_TO $ENV_BRANCH --no-ansi -n)
echo $ACLI
TASK_UUID=$(echo $ACLI | jq -r '.notification')
echo $TASK_UUID
acli app:task-wait $TASK_UUID

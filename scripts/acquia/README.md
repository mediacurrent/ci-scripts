# Acquia Deploy Scripts

This set of scripts can be run in `bitbucket-pipelines.yml` for updating an instance within Acquia and deploying new code to a particular environment.

## Prerequisites

* Though not required it is highly recommended to enable Acquia Cloud Actions when using these scripts. We reccomend the following to be enabled:

  * Backup all databases
  * Run database updates
  * Import configuration
  * Clear all Drupal caches
  * Clear all Varnish caches

  These can be configured under each envrionment under Configuration > Cloud Actions on Acquia Cloud Next instances.

* You will need an Acquia Cloud API key for this to work. You'll also need to ensure your repository has the following environment variables configured under "Repository Variables"
  * ACQUIA_API_KEY
  * ACQUIA_API_SECRET

## Configurate the scripts:

* `acquia-clone-db.sh` - Clones database rom one instance to another.
* `acquia-deploy.sh` - Deploys code to a particular instance using the code-switch command.

Copy and add these scripts to your `/scripts` directory from the root of the repository. Scripts can be customized as desired.

## Pipelines Setup

Here is an example of the bitbucket-pipelines.yml setup needed for leveraging this approach.

```
custom:
    backup-deploy:
      - variables:
        - name: ENV_BRANCH
          description: "Branch or tag to deploy. Prefix a tag with tags/."
        - name: DB_NAME
          default: {{ADD-DB-NAME-HERE}}
          description: "Database name to copy"
        - name: ENV_FROM
          default: {{ADD-PROD-ALIAS-HERE}}
          description: "Acquia Alias to copy environment from"
        - name: ENV_TO
          default: {{ADD-TEST-ALIAS-HERE}}
          description: "Acquia Alias to copy environment to"
      - step:
          name: Backup and Deploy to Acquia.
          script:
          - sh ./scripts/acquia-clone-db.sh
          - sh ./scripts/acquia-deploy.sh
    deploy-branch:
      - variables:
        - name: ENV_BRANCH
          description: "Branch or tag to deploy. Prefix a tag with tags/."
        - name: ENV_TO
          default: {{ADD-PROD-ALIAS-HERE}}
          description: "Acquia Alias to deploy code into."
      - step:
          name: Deploy to Acquia.
          script:
          - sh ./scripts/acquia-deploy.sh
```

## Usage

Once the Pipelines are setup they can be run by navigating to the "Pipelines" section of the repository and choosing Run pipeline in the top right.

These can only be run manually since they accept variables upon running.

## Known Issues

* When setting custom variables Pipelines doesn't retain the value used. If a pipeline needs to reun the variables chosen need to be reset.

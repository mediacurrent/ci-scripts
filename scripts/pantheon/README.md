# Pantheon Deploy Scripts

This set of scripts can be run in `bitbucket-pipelines.yml` for updating an instance within Acquia and deploying new code to a particular environment.

## Configurate the scripts:

* `pantheon-clone-db.sh` - Clones database rom one instance to another.
* `pantheon-deploy.sh` - Deploys code to a particular instance using the env:deploy command.

Copy and add these scripts to your `/scripts` directory from the root of the repository. Scripts can be customized as desired.

## Pipelines Setup

Here is an example of the bitbucket-pipelines.yml setup needed for leveraging this approach.

```
pipelines:
  custom:
    backup-deploy:
      - variables:
        - name: ENV_FROM
          default: {{ADD-SITE-ALIAS-HERE}}.live
          description: "Pantheon terminus alias to copy environment from. Should be the entire alias. '{{ADD-SITE-ALIAS-HERE}}.live' for example."
        - name: ENV_TO
          default: {{ADD-SITE-ALIAS-HERE}}.test
          description: "Pantheon enviornment to copy environment to."
        - name: ENV_FILES
          default: true
          description: "Clone files to environment"
          allowed-values:
            - true
            - false
      - step:
          name: Backup and Deploy to Pantheon.
          script:
          - sh ./scripts/pantheon/pantheon-clone-db.sh
          - sh ./scripts/pantheon/pantheon-deploy.sh
    deploy-branch:
      - variables:
        - name: ENV_TO
          default: {{ADD-SITE-ALIAS-HERE}}.live
          description: "Pantheon Alias to deploy code into."
        - name: ENV_NOTE
          description: "Commit message for deployment. Typically release tag number."
      - step:
          name: Deploy to Pantheon.
          script:
          - sh ./scripts/pantheon/pantheon-deploy.sh
```

## Quicksilver Setup

To run commands such as drush deploy after deployments a [Pantheon Quicksilver](https://docs.pantheon.io/guides/quicksilver) hook should be setup.

Add the follwoing to your `pantheon.yml` file:

```
  workflows:
  # Deploy to test and live.
  deploy:
    after:
      - type: webphp
        description: Update database and import configuration from .yml files
        script: scripts/pantheon/pantheon-drush-deploy.php
  # Sync code on dev and multidev.
  sync_code:
    after:
      - type: webphp
        description: Update database and import configuration from .yml files
        script: scripts/pantheon/pantheon-drush-deploy.php
```

## Usage

Once the Pipelines are setup they can be run by navigating to the "Pipelines" section of the repository and choosing Run pipeline in the top right.

These can only be run manually since they accept variables upon running.

## Known Issues

* When setting custom variables Pipelines doesn't retain the value used. If a pipeline needs to reun the variables chosen need to be reset.

#!/bin/bash

# Check if the 'terminus' command is available
if ! command -v terminus &> /dev/null; then
  mkdir -p ~/terminus && cd ~/terminus
  curl -L https://github.com/pantheon-systems/terminus/releases/download/3.6.0/terminus.phar --output terminus
  chmod +x terminus
  ./terminus self:update
  ln -s ~/terminus/terminus /usr/local/bin/terminus
fi

terminus auth:login --machine-token="${PANTHEON_TOKEN}" --no-interaction


if [ $ENV_FILES = "true" ]; then
  CLONE_FILES=''
  echo "Cloning entire instance into $ENV_TO"
else
  CLONE_FILES=" --db-only"
  echo "Cloning database only into $ENV_TO"
fi

# Get the environment out of the full site alias.
# Split on period and return second half.
SHORT_TO=$(echo "$ENV_TO" | cut -d'.' -f2)

# Run Terminus command to clone the instance
terminus env:clone-content ${ENV_FROM} ${SHORT_TO} ${CLONE_FILES} --no-interaction --updatedb	--cc --yes

# Check the exit status of the previous command
if [ $? -eq 0 ]; then
    echo "Instance cloned successfully."
else
    echo "Error: Instance cloning failed."
    exit 1;
fi

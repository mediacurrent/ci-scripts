#!/bin/bash

# Check if the 'terminus' command is available
if ! command -v terminus &> /dev/null; then
  mkdir -p ~/terminus && cd ~/terminus
  curl -L https://github.com/pantheon-systems/terminus/releases/download/3.6.0/terminus.phar --output terminus
  chmod +x terminus
  ./terminus self:update
  ln -s ~/terminus/terminus /usr/local/bin/terminus
fi

terminus auth:login --machine-token="${PANTHEON_TOKEN}" --no-interaction

echo "Backing up database on $ENV_TO."
#terminus backup:create --element database --keep-for 30 --no-interaction --yes -- $ENV_TO

if [ $ENV_FILES == "true" ]; then
  CLONE_FILES=' --sync-content'
  echo "Will sync content from production into $ENV_TO"
else
  CLONE_FILES=""
fi
echo "Switching codebase on $ENV_TO."
terminus env:deploy $ENV_TO --sync-content --note "${ENV_NOTE}" --cc --updatedb  --no-interaction --yes

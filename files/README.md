# README Template (Rename)

## Contents of This File

1. [**Project Overview**](#project-overview)
2. [**Environments**](#environments)
3. [**Quick Start**](#quick-start)
4. [**Development Workflow**](#development-workflow)
5. [**Repository Structure**](#repository-structure)
6. [**Testing**](#testing)
7. [**Deployment Process**](#deployment-process)
8. [**Additional Notes**](#additional-notes-and-resources)

---

## Project Overview

A description of the project and any goals/items to note about the project.

---

## Environments

| Environment | URL | Notes |
| ----------- | ----- | ------ |
| Develop  | https://path-to-env      | Active Development and default branch |
| Staging/Testing | https://path-to-env | Regression testing and release envrionment |
| Live | https://path-to-env | Live Production Instance. |

---

## Quick Start

This project requires a number of technologies to spin up locally:

* [DDEV](https://ddev.readthedocs.io/en/stable/users/install/ddev-installation/)
* [NVM](https://github.com/nvm-sh/nvm)
* [Composer](https://getcomposer.org/doc/00-intro.md)
* [Git](https://github.com/git-guides/install-git)

Once you have the required technologies:

1. Clone the project

    ```bash
    git clone {{ git_remote_url }}
    cd {{ bitbucket_project }}
    ```

2. Start and configure DDEV

    ```bash
    ddev config
    ddev start
    ddev auth ssh
    ```

3. (First-time only) Run the build script

    ```bash
    ./scripts/build.sh
    ```

4. Install the latest dev DB

    ```bash
    ddev pull acquia --skip-files
    ```

5. Follow the instructions in the [Theme README](./docs)(Update Link) to build the FE.

6. Log in

    ```bash
    ddev . drush uli
    ```


For more detail, please see the [Local Development guide](./docs/01-local-environment.md).

For information on the development workflow, check out the [Development Workflow guide](./docs/02-development-workflows.md).

---

## Repository Structure

This repository contains the codebase, configuration files, custom modules/themes, and deployment scripts for the <project-name>. While the repository is artifact-based, it supports configuration management for seamless environment synchronization.

- ```./docroot```: The Drupal root directory containing the site files.
- ```./tests```: Contains the automated testing suite for the site.
- ```./scripts```: Contains build and deployment scripts.
- ```./docs```: Contains documentation and guides for the project.

---

## Testing

### WDIO Testing

The project is equipped with a 100% automated testing suite using WebDriverIO. This suite includes tests for various aspects of the site, such as user login, critical path testing, and visual regression testing.

To begin testing with WebdriverIO, checkout the [README Guide](./tests/qa/README.md) for WDIO.

### Run coding standards tests.

*NOTE* Tests will not run until modules are in the "web/modules/custom" directory.

- phpcs - `./tests/code-sniffer.sh ./docroot`
- phpcbf - `./tests/code-fixer.sh ./docroot`

### Drupal-check custom development for Upgrade readiness.

*NOTE* Checks will not run until modules are in the "docroot/modules/custom" directory.

- `./vendor/mediacurrent/ci-tests/tests/drupal-check.sh docroot`

### Run a11y tests.

*NOTE* Requires [pa11y](https://github.com/pa11y/pa11y#command-line-interface)

- `./tests/pa11y/pa11y-review.sh https://example.ddev.site`

### OWASP Zap Baseline Scan.

- `docker run --net=ddev_default -v $(pwd):/zap/wrk/:rw -t owasp/zap2docker-weekly zap-baseline.py -d -c owasp-zap.conf -p owasp-zap-progress.json -t https://ddev-<projectname>-web`

### GrumPHP

- [GrumPHP](https://github.com/phpro/grumphp) will run some tests on code to be committed. The file grumphp.yml is used to configure.
	- Coding Standards
	- Deny committing a list of debug keywords
	- json and yaml linting
	- Composer lock file validation
	- Enlightn Security Checker

---

## Deployment Process

The <project-name> application is deployed to <environment>. The deployment process involves several steps that can be followed within the [Release Management Guide](./docs/release-management.md)(Update Link).

---

## Additional Notes and Resources

- [Local Development guide](./docs/01-local-environment.md)
- [Development Workflow guide](./docs/02-development-workflows.md)
- [Release Management Guide](./docs/release-management.md)(Update Link)
- [Troubleshooting](./docs/05-troubleshooting.md)
- [Using Composer](https://www.drupal.org/docs/develop/using-composer) with Drupal.



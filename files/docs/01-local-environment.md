# Local Development Guide

This guide explains how to set up and work with the a Drupal site in a local development environment using **DDEV**.

---

## Table of Contents

- [Local Development Guide](#local-development-guide)
  - [Table of Contents](#table-of-contents)
  - [Requirements](#requirements)
  - [Setting up a DDEV-Local Environment](#setting-up-a-ddev-local-environment)
    - [Install DDEV-Local](#install-ddev-local)
    - [Install Composer](#install-composer)
    - [Clone the Project](#clone-the-project)
    - [Configure DDEV](#configure-ddev)
    - [Configure DDEV](#configure-ddev-1)
  - [Building the Site](#building-the-site)
  - [Working with Acquia](#working-with-acquia)
    - [Authorize SSH](#authorize-ssh)
    - [Add Acquia API Keys](#add-acquia-api-keys)
    - [Importing a Database](#importing-a-database)
  - [Working with Pantheon](#working-with-pantheon)
    - [Authorize SSH](#authorize-ssh-1)
    - [Add Pantheon API Keys](#add-pantheon-api-keys)
    - [Importing a Database](#importing-a-database-1)
  - [Development Settings](#development-settings)
  - [Compiling the Theme](#compiling-the-theme)
  - [Useful Commands](#useful-commands)

---

## Requirements

- [DDEV](https://ddev.readthedocs.io/en/stable/)
- [Composer](https://getcomposer.org/)
- Access to Bitbucket repository for cloning the project.
- [NVM](https://github.com/nvm-sh/nvm)
- [Git](https://github.com/git-guides/install-git)

---

## Setting up a DDEV-Local Environment

### Install DDEV-Local

Follow the installation steps at [DDEV documentation](https://ddev.readthedocs.io/en/stable/).

### Install Composer

Ensure Composer is installed on your host machine:

- macOS: `brew install composer`
- Other platforms: [Install Composer](https://getcomposer.org/download/).

### Clone the Project

Clone the project from Bitbucket:

```bash
git clone git@bitbucket.org:mediacurrent/{{ PROJECT SLUG }}.git
cd {{ PROJECT SLUG }}
```

### Configure DDEV

Run the following command and accept all defaults:

```bash
ddev config
```

### Configure DDEV

Start the DDEV containers:

```bash
ddev start
```

---

## Building the Site

Run the build script to set up the site:
```bash
./scripts/build.sh
```

This script performs the following:

- Runs composer install to install dependencies.
- Runs drush site-install to set up a clean build of the site.

---

## Working with Acquia

### Authorize SSH

To enable SSH commands within the DDEV environment (e.g., ddev pull acquia), authorize your SSH key:

```bash
ddev auth ssh
```

### Add Acquia API Keys

Add Acquia API credentials to DDEVâ€™s global configuration:

- Follow the [Acquia API Key documentation](https://docs.acquia.com/acquia-cloud-platform/develop-apps/api/auth#cloud-generate-api-token).
- Add the keys as described in the [DDEV Acquia integration guide](https://ddev.readthedocs.io/en/latest/users/providers/acquia/).

### Importing a Database

1. Pull the latest database from Acquia (Requires SSH access to Acquia environments.):

    ```bash
    ddev pull acquia --skip-files
    ```

2. Alternatively, download a backup manually and import:

    ```bash
    ddev import-db --file=backup-name.sql.gz
    ```

---

## Working with Pantheon

### Authorize SSH

To enable SSH commands within the DDEV environment (e.g., ddev pull pantheon), authorize your SSH key:

```bash
ddev auth ssh
```

### Add Pantheon API Keys

Get your Pantheon machine token:

   * Log in to your Pantheon Dashboard and (Generate a Machine Token)[https://docs.pantheon.io/machine-tokens] for DDEV to use.
   * Add the API token to the web_environment section in your global DDEV configuration at ~/.ddev/global_config.yaml.

  ```yml
    web_environment:
        - TERMINUS_MACHINE_TOKEN=your_token
  ```

### Importing a Database

1. Pull the latest database from Pantheon

    ```bash
    ddev pull pantheon --skip-files
    ```

2. Alternatively, download a backup manually and import:

    ```bash
    ddev import-db --file=backup-name.sql.gz
    ```

---

## Development Settings

To use local development settings, copy the example local settings file:

```bash
cp ./web/sites/example.settings.local.php ./web/sites/default/settings.local.php
```

This file:

- Disables Drupal's built-in caching.
- Activates sites/development.services.yml for further customization.

Note: The initial ./scripts/build.sh copies this file into place

---

## Compiling the Theme

Changes to the theme will not render correctly until the theme build process is completed. From the appropriate theme directory, run:

```bash
nvm install
npm ci
npm run build
```

For more detailed instructions, refer to the Theme READMEs:

- [Theme README]({{ LINK HERE }})

---

## Useful Commands

Common DDEV Commands

- Start DDEV: `ddev start`
- Clear caches: `ddev drush cr`
- Import database: `ddev import-db --file=filename.sql.gz`
- Pull database from Acquia: `ddev pull acquia --skip-files`
- Pull database from Pantheon: `ddev pull pantheon --skip-files`
- Login to the site: ddev drush uli

Common Composer Commands

- Install dependencies: `ddev composer install`
- Add a module: `ddev composer require drupal/<module-name>`

Common Drush Commands

- Export configuration: `ddev drush cex`
- Import configuration: `ddev drush cim`
- List site aliases: `ddev drush sa`

---

For additional information and documentation, check out the main site [README](../README.md).
# Feature A Documentation

## Table of Contents

- [Feature A]()

## Goals

A short summary of the goals/objectives of this feature. Should be more user facing versus technical facing to understand "why" this feature exists.

## Architectural Summary

A description of the architecture including any assets/infographics that showcase the product.

## Authoring Summary

How does a user interact and leverage this feature.

## Technical Details

How the feature is put together and any details associated to it

## FAQ/Debugging Steps

Details of common problems, debugging or FAQs that come up associated to the feature.
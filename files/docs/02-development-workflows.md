# Development Workflow

## Contents of This File
- [Development Workflow](#development-workflow)
  - [Contents of This File](#contents-of-this-file)
  - [Workflow Overview](#workflow-overview)
  - [Updating Your Local Environment](#updating-your-local-environment)
  - [Developing New Updates](#developing-new-updates)
    - [Starting a New Feature Branch](#starting-a-new-feature-branch)
    - [Commit Message Formatting](#commit-message-formatting)
    - [Completing a Feature](#completing-a-feature)
  - [Updating Site-Owned Configuration](#updating-site-owned-configuration)
  - [Adding New Dependencies](#adding-new-dependencies)
    - [Applying Patches](#applying-patches)
  - [Development Settings](#development-settings)
  - [Drush commands you should know](#drush-commands-you-should-know)

---

## Workflow Overview

* [Use Composer](https://www.drupal.org/docs/develop/using-composer/using-composer-to-manage-drupal-site-dependencies#managing-contributed) to add
  3rd party dependencies and patches.
* Write custom modules, themes etc. to the ./docroot/ directory.
* Run `ddev . drush cex` to export Drupal configuration to the
  ./config/sync folder.
* Before starting a new ticket, you may need to import the latest production DB,
  check out the "develop" branch, and run a config-export to capture any
  production settings that may have been changed by others.

## Updating Your Local Environment

Before beginning a new task, it is often beneficial to ensure you are using the latest version of the site. Follow these steps:

1. Sync your local branch with `origin/develop`:

    ```bash
    git checkout develop
    git pull --rebase
    ```

2. Install any new dependencies:

    ```bash
    composer install
    ```

3. Update your local database and clear caches:

    ```bash
    ddev drush cr
    ddev drush updb
    ddev drush cim
    ```

These commands will update your local database in the same way the live database is updated during a release. If there are no updates or configuration changes, the commands will report that everything is up to date.

---

## Developing New Updates

The project uses git-flow for development and release management. Features are merged into the develop branch through pull requests with reviews after passing UAT.

### Starting a New Feature Branch

To start work on a new feature, create a feature branch using the naming convention:
feature/ABC-123--optional-short-description where ABC-123 is the Jira ticket number.

1. Ensure your local develop branch is up to date:

    ```bash
    git checkout develop
    git pull --rebase
    ```

2. Create a new branch:

    ```bash
    git checkout -b feature/optional-ABC-123--short-desc
    ```

### Commit Message Formatting

Follow this format for commit messages:

```text
ABC-123 A short title for what the commit does
# A blank line here. Note: Limit the subject line to 50 characters, if possible
Optionally, a longer description of the commit and any notes that may
be helpful in the future, with line breaks at around 72 characters.
```

Example:

```text
ABC-123 Fix typo in Basic Page description
```

For more details, refer to the [Git commit documentation](https://git-scm.com/docs/git-commit#_discussion).

### Completing a Feature

Once the feature is ready for review:

1. Push your feature branch to Bitbucket:

    ```bash
    git push -u origin feature/ABC-123--short-desc
    ```

2. Create a pull request (PR) using the link provided in your terminal, or navigate to the branch page in Bitbucket.
   > __Note__: Make sure to select the â€œClose branchâ€ option.

---

## Updating Site-Owned Configuration

1. **Work**: Make changes through the Drupal UI or other appropriate means.
2. **Save**: Export the configuration to code (This will save the database configuration to the sync directory):

    ```bash
    ddev drush cex -y
    ```

3. Commit: Add and commit the configuration changes to the repository:

    ```bash
    git add .
    git commit
    ```

   > __Note__: Make sure to only add the relevant files to the commit.

---

## Adding New Dependencies

1. Add the module using Composer:

    ```bash
    ddev composer require drupal/[module-name]
    ```

__Note__: Use the recommended version dependency and command on the Drupal module release page.

2. Enable the module:

    ```bash
    ddev drush pm:enable [module-name]
    ```

3. Export the module configuration:

    ```bash
    ddev drush cex
    ```
4. Commit your changes

---

### Applying Patches

To apply patches, use the [composer-patches](https://github.com/cweagans/composer-patches) plugin.
1. Add the patch details to the extra section of composer.json:

    ```json
    "extra": {
      "patches": {
         "drupal/project": {
           "Patch description": "URL or local path to patch"
         }
       }
    }
    ```

2. Apply the patch by running:

    ```bash
    composer install
    ```

## Development Settings
* The settings.local.php file contains settings for customizing the development environment.  This disables Drupal's built in caching and additionally activates sites/development.services.yml for further customizing the development environment.

## Drush commands you should know

* Use `ddev drush uli` to login to your local installation.
* `ddev drush cr` to flush the drupal cache
* `ddev drush cex` to export config changes
* `ddev drush cim` to import config changes

---

For additional information and documentation, check out the main site [README](../README.md).
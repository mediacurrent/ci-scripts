# ACQUIA SITE Release Workflow

### Variables:
| Key | Value | Notes |
| ----------- | ---- | ---- |
| {{ADD-SITE-ALIAS-HERE}}  | abc | Acquia Drush Alias. No `@` nor `.prod` |
| {{ADD-REPO-SLUG-HERE}} | abc_repo_name | Slug for repository |
| {{ADD-DB-NAME-HERE}} | database_name | Database name for cloning |

Search variables and replace as required throughout document.

## Contents of This File

- [ACQUIA SITE Release Workflow](#acquia-site-release-workflow)
    - [Variables:](#variables)
  - [Contents of This File](#contents-of-this-file)
  - [Release Steps/Deployment](#release-stepsdeployment)
    - [Create Release Branch](#create-release-branch)
    - [Capture any configuration changes from production.](#capture-any-configuration-changes-from-production)
    - [Deploy to Acquia Staging](#deploy-to-acquia-staging)
    - [Check Production Configurations](#check-production-configurations)
    - [Create Tag](#create-tag)
    - [Deploy to Acquia Prod](#deploy-to-acquia-prod)
    - [Clean up release](#clean-up-release)
  - [Creating an Acquia Token](#creating-an-acquia-token)


## Release Steps/Deployment

_Example Release is 3.5.0. Update that version number accordingly._

- Ensure main and develop branches are up to date in local environment

    ```
    git checkout main; git pull --rebase
    git checkout develop; git pull --rebase
    ```

- Ensure all tickets are tagged with the proper fixVersion in Jira. Validate this list and ensure all tickets expected from fixVersion in Jira are represented and validate any unexpected issues.

    ```
    git log --pretty=oneline main..develop | grep -e '[A-Z]\+-[0-9]\+' -o | sort -u
    ```

### Create Release Branch

Merge relevant commits into a branch. Typically this is the most recent develop branch. The release branch number should be the same as your Fix Version.

  ```
  # Ensure you are on develop branch.
  git checkout develop;
  # Fetch/Rebase to ensure you have latest code from develop.
  git pull --rebase;
  # Create a release branch based upon develop
  git checkout -b release/3.5.0
  ```

Validate all code in the release branch aligns to what is expected for the release. Typically compare to the main branch or the previous release tag.

Example:  `git log main..release/3.5.0 --oneline`

### Capture any configuration changes from production.

Run `ddev drush @{{ADD-SITE-ALIAS-HERE}}.prod config:status` and log if there are any configuration changes in production.

If there are any configuration changes then capture those from production.

-  Capture all config from production in a new commit.

  ```
  git checkout develop; git pull --rebase;
  git checkout main; git pull --rebase;
  # Create a new config branch to capture production configs:
  git checkout main; git checkout -B config/3.5.0
  ```

- Update local with live db:

  * Update `.ddev/config.ym`l` to prevent drush deploy from running in post-import-db
  * `ddev pull acquia --skip-files`
  * Run `ddev drush updb -y` to capture and database update
  * Verify configs match production
  * Export configs: `ddev drush config:export`
  * Add and commit changes:

    ```
    git add config;
    git commit -m 'Capturing configs for 3.5.0 release.'
    ```

 - Merge to Release Branch:

    ```
    git checkout release/3.5.0
    git pull --rebase;
    git merge config/3.5.0
    ```

### Deploy to Acquia Staging

  - Push to Acquia origin to allow Bitbucket Pipeline to run.

    ```
    git push origin release/3.5.0
    ```

    Bitbucket Pipelines will run a deploy that builds an artifact build and pushes to Acquia as release/3.5.0. You can check status of the pipeline at: [mediacurrent/{{ADD-REPO-SLUG-HERE}}](https://bitbucket.org/mediacurrent/{{ADD-REPO-SLUG-HERE}}/pipelines).

  - Switch code on the 'Stage' instance to your release branch: release/3.5.0:

    * In [Bitbucket](https://bitbucket.org/mediacurrent/{{ADD-REPO-SLUG-HERE}}/pipelines) choose "Pipelines" and "Run Pipeline"
    * Choose your branch "release/3.5.0" then choose `custom: backup-deploy` pipeline.
    * Set the following values:

      *  `ENV_BRANCH`: `release/3.5.0`
      *  `DB_NAME`: `{{ADD-DB-NAME-HERE}}` (Should be default)
      *  `ENV_FROM`: `{{ADD-SITE-ALIAS-HERE}}.prod` (Should be default)
      *  `ENV_TO`: `{{ADD-SITE-ALIAS-HERE}}.test` (Should be default)

    * Click "Run" and monitor the progress of the pipeline. During this step the pipeline will take a copy of the production database, move it to staging and deploy your release branch. Then with [Acquia Cloud Actions](https://cloud.acquia.com/a/environments/86814-1f5bdbd2-da41-4d8f-8834-79b9db850abe/config/cloud-actions) the databse updates will be run, config imported and caches cleared.

    * If you need to manually run any commands you can do those with Drush (but they should run automatically):

    ```
    # Run DB updates and import all config changes.
    ddev drush @{{ADD-SITE-ALIAS-HERE}}.test deploy -y
    ```

  - Test with QA or review any issues with Client. Ensure to smoketest release notes above.

### Check Production Configurations

  - Check that any production configuration changes are captured prior to tagging a release and deploy.

    ```
    ddev drush @{{ADD-SITE-ALIAS-HERE}}.prod config:status
    ```

  - Compare to configs status above.  If there are any differences in configs capture those into the release branch as described under "Capture all config from production in a new commit"

### Create Tag

  - Once the release is verified and ready to deploy a tag should be created. The tag should be created based on the main branch after the release branch has been merged in. To do this follow a git flow process:

    ```
    # Ensure we start on the release branch.
    git checkout release/3.5.0;

    # Ensure release branch is up to date.
    git fetch; git rebase origin/release/3.5.0;

    # Ensure main branch is up to date.
    git checkout main; git pull --rebase;

    # Merge release branch into main.
    git merge release/3.5.0

    # Tag Release.
    git tag 3.5.0

    # Ensure develop is up to date for merge into develop.
    git checkout develop; git fetch; git rebase origin/develop;

    # Merge main with develop.
    git merge main - Merge main with develop

    # Push all branches/tags to orign.
    git push origin main; git push origin develop; git push origin --tags; - Push all branches/tags to origin

### Deploy to Acquia Prod

- Bitbucket Pipelines will run a deploy that builds an artifact build and pushes to Acquia as tags/3.5.0. You can check status of the pipeline at: [mediacurrent/{{ADD-REPO-SLUG-HERE}}](https://bitbucket.org/mediacurrent/{{ADD-REPO-SLUG-HERE}}/pipelines).
- Run Pipeline to deploy to production.

    * In [Bitbucket](https://bitbucket.org/mediacurrent/{{ADD-REPO-SLUG-HERE}}/pipelines) choose "Pipelines" and "Run Pipeline"
    * Choose your branch "main" then choose `custom: deploy-branch` pipeline.
    * Set the following values:

      *  `ENV_BRANCH`: `tags/3.5.0`
      *  `ENV_TO`: `{{ADD-SITE-ALIAS-HERE}}.prod` (Should be default)

    * Click "Run" and monitor the progress of the pipeline. During this step the pipeline will take a copy of the production database, move it to staging and deploy your release branch. Then with Acquia Cloud Actions the databse updates will be run, config imported and caches cleared.

    * If you need to manually run any commands you can do those with Drush (but they should run automatically):

      ```
      # Run DB updates and import all config changes.`
      ddev drush @{{ADD-SITE-ALIAS-HERE}}.prod deploy -y
      ```

- Test with QA or review any issues with Client. Ensure to smoketest release notes above.
- This site has had problems with Google Tag Manager not tracking. Upon release, please:

  * Use the Analytics Debugger Chrome extension to validate that GTM is successfully tracking.

### Clean up release

- Ensure tickets are updated accordingly in Jira.
- "Release" the fixVersion in Jira creating release notes.

    * Optionally change formatting options to include testing instructions,post-release instructions and release notes into the release notes.
- Complete any Post Release Instructions.

---


## Creating an Acquia Token

In order to use `ddev pull acquia` you'll need to have an Acquia token created.

1. Create an API token via Acquia under Account Settings > API Tokens. See documentation at [Acquia](https://docs.acquia.com/cloud-platform/develop/api/auth/). Make sure to save this key in a secure location.
2. Update your `~/.ddev/global_config.yaml` file with the API key from step 1. Details provided at [DDEV](https://ddev.readthedocs.io/en/stable/users/providers/acquia/)/. Example `~/.ddev/global_config.yaml`:


```
  web_environment:
- TERMINUS_MACHINE_TOKEN=TOKEN_HERE
- ACQUIA_API_KEY=KEY_HERE
- ACQUIA_API_SECRET=SECRET_HERE
```

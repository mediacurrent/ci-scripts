# Automated Testing

## Contents of This File

- [Automated Testing](#automated-testing)
  - [Contents of This File](#contents-of-this-file)
  - [Run coding standards tests.](#run-coding-standards-tests)
  - [Drupal-check custom development for major version readiness.](#drupal-check-custom-development-for-major-version-readiness)
    - [Run phpunit tests.](#run-phpunit-tests)
    - [Running Critical Path Test](#running-critical-path-test)
    - [Running Regression Tests](#running-regression-tests)
    - [Run VRT.](#run-vrt)
    - [Run a11y tests.](#run-a11y-tests)
    - [OWASP Zap Baseline Scan.](#owasp-zap-baseline-scan)
    - [GrumPHP](#grumphp)


## Run coding standards tests.

*NOTE* Tests will not run until modules are in the "web/modules/custom" directory.

- phpcs - `./tests/code-sniffer.sh ./web`
- phpcbf - `./tests/code-fixer.sh ./web`

## Drupal-check custom development for major version readiness.

*NOTE* Checks will not run until modules are in the "web/modules/custom" directory.

- `./vendor/mediacurrent/ci-tests/tests/drupal-check.sh web`

### Run phpunit tests.

- unit tests - `ddev composer robo test:phpunit-tests`
- kernel and functional tests - `ddev composer robo test:phpunit-tests -- --filter="/Kernel|Functional/"`

### Running Critical Path Test

From the testing directory (```cd ./tests/qa```):

```npm run wdio:critical```

### Running Regression Tests

From the testing directory (```cd ./tests/qa```):

```npm run wdio```
NOTE: this will run all tests defined within the application.

### Run VRT.

  From the testing directory (```cd ./tests/qa```):

  ```npm run wdio:vrt```

### Run a11y tests.

*NOTE* Requires [pa11y](https://github.com/pa11y/pa11y#command-line-interface)

- `./tests/pa11y/pa11y-review.sh https://example.ddev.site`

### OWASP Zap Baseline Scan.

- `docker run --net=ddev_default -v $(pwd):/zap/wrk/:rw -t owasp/zap2docker-weekly zap-baseline.py -d -c owasp-zap.conf -p owasp-zap-progress.json -t https://ddev-<projectname>-web`

### GrumPHP

* [GrumPHP](https://github.com/phpro/grumphp) will run some tests on code to be committed. The file grumphp.yml is used to configure.
	* Coding Standards
	* Deny committing a list of debug keywords
	* json and yaml linting
	* Composer lock file validation
	* Composer Audit

# PANTHEON SITE Release Workflow

### Variables:
| Key                     | Value         | Notes               |
| ----------------------- | ------------- | ------------------- |
| {{ADD-SITE-ALIAS-HERE}} | abc     | Pantheon terminus alias. Do not include `@` symbol or the environment identifier (ex: `.live`|
| {{ADD-REPO-SLUG-HERE}}  | abc_repo_name | Slug for repository |

Search variables and replace as required throughout document.

## Contents of This File

- [PANTHEON SITE Release Workflow](#pantheon-site-release-workflow)
    - [Variables:](#variables)
  - [Contents of This File](#contents-of-this-file)
  - [Release Steps/Deployment](#release-stepsdeployment)
    - [Create Release Branch](#create-release-branch)
    - [Capture any configuration changes from production.](#capture-any-configuration-changes-from-production)
    - [Deploy to Pantheon Dev](#deploy-to-pantheon-dev)
    - [Deploy to Pantheon Test](#deploy-to-pantheon-test)
    - [Create Tag](#create-tag)
    - [Deploy to Pantheon Prod](#deploy-to-pantheon-prod)
    - [Clean up release](#clean-up-release)


## Release Steps/Deployment

_Example Release is 3.5.0. Update that version number accordingly._

- Ensure master and develop branches are up to date in local environment

    ```
    git checkout master; git pull --rebase
    git checkout develop; git pull --rebase
    ```

- Ensure all tickets are tagged with the proper fixVersion in Jira. Validate this list and ensure all tickets expected from fixVersion in Jira are represented and validate any unexpected issues.

    ```
    git log --pretty=oneline master..develop | grep -e '[A-Z]\+-[0-9]\+' -o | sort -u
    ```

### Create Release Branch

Merge relevant commits into a branch. Typically this is the most recent develop branch. The release branch number should be the same as your Fix Version.

  ```
  # Ensure you are on develop branch.
  git checkout develop;
  # Fetch/Rebase to ensure you have latest code from develop.
  git pull --rebase;
  # Create a release branch based upon develop
  git checkout -b rc-3.5.0
  ```

Validate all code in the release branch aligns to what is expected for the release. Typically compare to the master branch or the previous release tag.

Example:  `git log master..rc-3.5.0 --oneline`

### Capture any configuration changes from production.

Run `terminus drush {{ADD-SITE-ALIAS-HERE}}.live -- config:status` and log if there are any configuration changes in production.

If there are any configuration changes then capture those from production.

  -  Capture all config from production in a new commit.

    ```
    git checkout develop; git pull --rebase;
    git checkout master; git pull --rebase;
    # Create a new config branch to capture production configs:
    git checkout master; git checkout -B config/3.5.0
    ```

  - Update local with live db:

    Alternatively you may choose to captrue the config changes manually from production. This is preferred when only a few changes are seen.

    * `ddev pull pantheon --skip-files --skip-hooks`
    * Run `ddev drush updb -y` to capture and database update
    * Verify configs match production
    * Export configs: `ddev drush config:export`
    * Add and commit changes:

      ```
      git add config;
      git commit -m 'Capturing configs for 3.5.0 release.'
      ```

   - Merge to Release Branch:

      ```
      git checkout rc-3.5.0
      git pull --rebase;
      git merge config/3.5.0
      ```

### Deploy to Pantheon Dev

  - Merge to git master

    ```
    git checkout rc-3.5.0;
    git pull --rebase;
    git checkout master;
    git pull --rebase;
    git merge rc-3.5.0;
    git push origin master;
    ```

    Bitbucket Pipelines will run a deploy that builds an artifact and pushes to Pantheon. You can check status of the pipeline at: [mediacurrent/{{ADD-REPO-SLUG-HERE}}](https://bitbucket.org/mediacurrent/{{ADD-REPO-SLUG-HERE}}/pipelines).

### Deploy to Pantheon Test

  - When ready to deploy to test run the Pipeline for deployment

    * In [Bitbucket](https://bitbucket.org/mediacurrent/{{ADD-REPO-SLUG-HERE}}/pipelines) choose "Pipelines" and "Run Pipeline"
    * Choose your branch "master" then choose `custom: backup-deploy` pipeline.
    * The default values should work:

      *  `ENV_FROM`: `{{ADD-SITE-ALIAS-HERE}}.live` (Should be default)
      *  `ENV_TO`: `{{ADD-SITE-ALIAS-HERE}}.test` (Should be default)
      *  `ENV_FILES`: `true` (Should be default)

    * Click "Run" and monitor the progress of the pipeline. During this step the pipeline will take a copy of the production database, move it to test and deploy your master branch. Then with [Quicksilver Hooks](https://docs.pantheon.io/guides/quicksilver) the databse updates will be run, config imported and caches cleared.

    * If you need to manually run any commands you can do those with Drush (but they should run automatically):

    ```
    # Run DB updates and import all config changes.
    terminus drush {{ADD-SITE-ALIAS-HERE}}.test deploy -y
    ```

  - Test with QA or review any issues with Client. Ensure to smoketest release notes from Jira.

### Create Tag

  - Once the release is verified and ready to deploy a tag should be created. The tag should be created based on the master branch after the release branch has been merged in. To do this follow a git flow process:


    ```
    # Ensure we start on the master branch.
    git checkout master;

    # Ensure release branch is up to date.
    git fetch; git rebase origin/master;e;

    # Tag Release.
    git tag 3.5.0

    # Ensure develop is up to date for merge into develop.
    git checkout develop; git fetch; git rebase origin/develop;

    # Merge master with develop.
    git merge master - Merge master with develop

    # Push all branches/tags to orign.
    git push origin master; git push origin develop; git push origin --tags; - Push all branches/tags to origin
    ```


### Deploy to Pantheon Prod

- Bitbucket Pipelines will run a deploy that builds an artifact build and pushes to PantheonYou can check status of the pipeline at: [mediacurrent/{{ADD-REPO-SLUG-HERE}}](https://bitbucket.org/mediacurrent/{{ADD-REPO-SLUG-HERE}}/pipelines).
- Run Pipeline to deploy to production.

    * In [Bitbucket](https://bitbucket.org/mediacurrent/{{ADD-REPO-SLUG-HERE}}/pipelines) choose "Pipelines" and "Run Pipeline"
    * Choose your branch "master" then choose `custom: deploy-branch` pipeline.
    * Set the following values:

      *  `ENV_TO`: `{{ADD-SITE-ALIAS-HERE}}.live` (Should be default)
      *  `ENV_NOTE`: `Release 3.5.0` (Note for the commit message)

    * Click "Run" and monitor the progress of the pipeline.

    * If you need to manually run any commands you can do those with Drush (but they should run automatically):

      ```
      # Run DB updates and import all config changes.
      terminus drush {{ADD-SITE-ALIAS-HERE}}.test deploy -y
      ```

- Test with QA or review any issues with Client. Ensure to smoketest release notes above.

  * Use the Analytics Debugger Chrome extension to validate that GTM is successfully tracking.

### Clean up release

- Ensure tickets are updated accordingly in Jira.
- "Release" the fixVersion in Jira creating release notes.

    * Optionally change formatting options to include testing instructions,post-release instructions and release notes into the release notes.
- Complete any Post Release Instructions.
